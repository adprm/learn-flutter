import 'package:flutter/material.dart';

const Color primaryColor = Color(0xff1A77AA);
const Color kBgColor = Color(0xffF9FAFB);
const Color kPlaceholder = Color(0xff79747E);